Install:
```
$ npm install && bower install && bundle install
```

Build development package:
```
$ cd build
$ gulp build
```

Build distribution package:
```
$ cd build
$ gulp build:dist
```
