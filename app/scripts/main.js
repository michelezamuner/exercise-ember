'use strict';
var config = require('./config.js');
var load = require('./load.js');
var deps = [ config.lib.jquery, config.lib.handlebars, config.lib.ember, config.lib['ember-data'] ];
load(deps, function() {
    require('./application.js');
    require('./router.js');
    require('./models/todo.js');
    require('./controllers/todos_controller.js');
    require('./controllers/todo_controller.js');
    require('./views/edit_todo_view.js');
});
