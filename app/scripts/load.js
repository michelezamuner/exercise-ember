/* global __DEV__ */
/* global __DIST__ */
'use strict';
var script = require('scriptjs');

/*
 * Load resources one at a time, waiting for each to be
 * downloaded or backed up by the local version before
 * passing to the next one.
 */
function loadResources(resources, callback, i) {
    if (i === undefined) { i = 0; }
    var resource = resources[i];
    script(resource.cdn, function() {
        try {
            window[resource.object] = resource.dist();
        } catch (e) {
            console.log('Cannot load ' + resource.object + ' from CDN. Loading local version...');
            resource.dev();
        }
        if (i < resources.length - 1) {
            loadResources(resources, callback, i + 1);
        } else {
            callback();
        }
    });
}

module.exports = function(resources, callback) {
    if (__DEV__) {
        for (var i = 0; i < resources.length; i++) {
            resources[i].dev();
        }
        callback();
    } else if (__DIST__) {
        loadResources(resources, callback);
    }
};
