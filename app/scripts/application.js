/* global Ember */
/* global DS */
'use strict';
window.Todos = Ember.Application.create();

var Todos = window.Todos;
Todos.ApplicationAdapter = DS.FixtureAdapter.extend();

Todos.TodosRoute = Ember.Route.extend({
  model: function() {
    return this.store.find('todo');
  }
});
