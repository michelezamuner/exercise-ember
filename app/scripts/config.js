'use strict';
module.exports = {
    'lib': {
        'jquery': {
            'object': 'jQuery',
            'dev': function() { require('script!jquery'); },
            'dist': function() { return require('jquery-cdn'); },
            'cdn': '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'
        },
        'handlebars': {
            'object': 'Handlebars',
            'dev': function() { require('script!handlebars'); },
            'dist': function() { return require('handlebars-cdn'); },
            'cdn': '//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.3.0/handlebars.min.js'
        },
        'ember': {
            'object': 'Ember',
            'dev': function() { require('script!ember'); },
            'dist': function() { return require('ember-cdn'); },
            'cdn': '//cdnjs.cloudflare.com/ajax/libs/ember.js/1.7.0/ember.min.js'
        },
        'ember-data': {
            'object': 'DS',
            'dev': function() { require('script!ember-data'); },
            'dist': function() { return require('ember-data-cdn'); },
            'cdn': '//cdnjs.cloudflare.com/ajax/libs/ember-data.js/1.0.0-beta.12/ember-data.min.js'
        }
    }
};
