'use strict';
var root = require('path').join(__dirname, '..');

module.exports = {
  css: root + '/public',
  sass: root + '/app/styles'
};
