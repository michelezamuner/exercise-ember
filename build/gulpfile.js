'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var root = require('path').join(__dirname, '..');

// Reset env vars
process.env.BUILD_DEV = false;
process.env.BUILD_DIST = false;

gulp.task('default', [ 'watch' ]);

gulp.task('build', [ 'views' ]);
gulp.task('build:dist', [ 'views:dist' ]);

gulp.task('clean', function(cb) {
    process.chdir(root);
    require('del')([
      'public/*',
      '!public/vendor',
      'public/.*'
    ], cb);
});

gulp.task('scripts', [ 'modernizr' ], function () {
    process.env.BUILD_DEV = true;
    return gulp.src(root + '/app/scripts/main.js')
        .pipe($.webpack(require('./webpack.config.js')))
        .pipe($.size())
        .pipe(gulp.dest(root + '/public/'));
});

gulp.task('scripts:dist', [ 'modernizr:dist' ], function() {
    process.env.BUILD_DIST = true;
    return gulp.src(root + '/app/scripts/main.js')
        .pipe($.webpack(require('./webpack.config.js')))
        .pipe($.uglify())
        .pipe($.size())
        .pipe(gulp.dest(root + '/public'));
});

gulp.task('modernizr', function() {
    return gulp.src(root + '/app/scripts/**/*.js')
        .pipe($.modernizr(require('./modernizr.config.js')))
        .pipe($.size())
        .pipe(gulp.dest(root + '/public'));
});

gulp.task('modernizr:dist', function() {
    return gulp.src(root + '/app/scripts/**/*.js')
        .pipe($.modernizr('modernizr.min.js', require('./modernizr.config.js')))
        .pipe($.uglify())
        .pipe($.size())
        .pipe(gulp.dest(root + '/public'));
});

gulp.task('styles', function() {
    return gulp.src(root + '/app/styles/**/*.scss')
        .pipe($.compass(require('./compass.config.js')))
        .pipe(gulp.dest(root + '/public'));
});

gulp.task('views', [ 'scripts', 'styles', 'images' ], function() {
    /*
     * gulp-inject, if called from outside public/,
     * writes links as ../public/script.js. To avoid
     * this we need to chdir to public/ first.
     */
    process.chdir(root + '/public');
    return gulp.src(root + '/app/*.html')
        .pipe($.inject(
            gulp.src([ './modernizr.js' ], { read: false }),
            { starttag: '<!-- inject:head:{{ext}} -->' }
        ))
        .pipe(gulp.dest(root + '/public'));
});

gulp.task('views:dist', [ 'scripts:dist', 'styles', 'images' ], function() {
    /*
     * gulp-inject, if called from outside public/,
     * writes links as ../public/script.js. To avoid
     * this we need to chdir to public/ first.
     */
    process.chdir(root + '/public');
    return gulp.src(root + '/app/*.html')
        .pipe($.inject(
            gulp.src([ './modernizr.min.js' ], { read: false }),
            { starttag: '<!-- inject:head:{{ext}} -->' }
        ))
        .pipe(gulp.dest(root + '/public'));
});

gulp.task('images', function() {
    return gulp.src(root + '/app/*.png')
        .pipe(gulp.dest(root + '/public'));
});

gulp.task('connect', function () {
    var connect = require('connect');
    var app = connect()
        .use(require('connect-livereload')({ port: 35729 }))
        .use(connect.static(root + '/public'))
        .use(connect.directory(root + '/public'))

    require('http').createServer(app)
        .listen(9000)
        .on('listening', function () {
            console.log('Started connect web server on http://localhost:9000');
        });
});

gulp.task('watch', ['connect'], function () {
    var server = $.livereload();

    gulp.watch([
        root + '/public/*.html',
        root + '/public/*.js',
    ]).on('change', function (file) {
        server.changed(file.path);
    });

    gulp.watch(root + '/app/scripts/**/*.js', ['scripts']);
    gulp.watch(root + '/app/*.html', ['html']);
});
