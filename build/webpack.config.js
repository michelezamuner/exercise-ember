var path = require('path');
var webpack = require('webpack');
var root = require('path').join(__dirname, '..');

module.exports = {
    externals: {
        'jquery-cdn': 'jQuery',
        'handlebars-cdn': 'Handlebars',
        'ember-cdn': 'Ember',
        'ember-data-cdn': 'DS'
    },
    module: {
        preLoaders: [
            {
                exclude: /node_modules|public\/vendor/,
                loader: 'jshint-loader'
            }
        ]
    },
    jshint: { devel: true },
    output: { filename: 'bundle.js' },
    resolve: {
        root: [ root + '/public/vendor' ]
    },
    plugins: [
        new webpack.ResolverPlugin(
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('bower.json', ['main'])
        ),
        new webpack.DefinePlugin({
            __DEV__: JSON.stringify(JSON.parse(process.env.BUILD_DEV || 'true')),
            __DIST__: JSON.stringify(JSON.parse(process.env.BUILD_DIST || 'false'))
        })
    ]
};
